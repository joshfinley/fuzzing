### Intro:

[AFL](http://lcamtuf.coredump.cx/afl/) is a fuzzing tool written by Michał Zalewski. Fuzzing is a vulnerability enumeration process that involves generating and testing possible malformed inputs against a software application with the goal of causing crashes, hangs, or otherwise undesirable behavior.

#### [transition to view of scrolling bugorama trophy case]

AFL has discovered dozens of critical vulnerabilities in all sorts of software, from internet explorer to Apples iOS kernel.

### Demonstration

#### [switch to terminal view]

To use AFL, begin by downloading the source code for afl and the application we want to fuzz.

Compile AFL.

Once installed, we can move to the target directory and compile the source code using afl's special compilers. This will inject instrumentation used by afl into the application.

Next we configure core dump notifications and begin fuzzing the program itself

AFL will now do its work fuzzing the target application. After letting the application run for a while, we can see that it starts generating crashes in the target application. If we investigate the crash and are able to succesfully  reproduce it, we may have an interesting vulnerability on our hands. Bug bounties for these sorts of vulnerabilities can range from a few hundred to many thousands of dollars.

#### [screen shows condensed clips of this]


