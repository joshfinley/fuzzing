# Fuzzing with AFL - Getting Started Guide

This tutorial will give a brief overview of the process needed to get started with fuzzing in AFL. I've chosen PHP 7.0.33 as the target source code for this project.

 Before starting, make sure you have the following prerequisites: a computer or virtual machine running Ubuntu 14.04 and plenty of patience. Use other operating systems or versions at your own peril.



## 1 - Environment setup

First, we need to prepare our machine for fuzzing. This includes creating the appropriate folders, installing required software, and downloading our target source code. For this tutorial, I've selected an older version of the php interpreter as the target.


Run these commands from your home folder (`/home/your_username/`):



``` bash
# create project directory
mkdir fuzzing
mkdir fuzzing/sources
cd fuzzing

# download LLVM and Clang
apt-get install -y clang llvm cmake tmux

# download and unpack AFL
wget http://lcamtuf.coredump.cx/afl/releases/afl-latest.tgz
tar -xvzf afl-latest.tgz

# download and unpack target source code
wget https://github.com/php/php-src/archive/php-7.0.33.tar.gz
tar -xvzf php-7.0.33.tar.gz

# cleanup
mv ./*.t* ./sources
```

at this point, your project directory should look like this:

```
afl-2.52b
php-src-php-7.0.33
sources
```

Now that we have all the source code that we need, we need to compile our fuzzer. Enter the `afl` directory with `cd afl*` and then run the following commands to build the source code into an executable and then install it on your system.



``` bash
make
cd llvm_mode
make
cd ..
make install
```

return to the project base directory with `cd ..`


We now need a few packages to make sure PHP builds correctly. Run the following:

``` bash
apt-get -y install build-essential git autoconf
apt-get -y install bison  libgmp-dev libxml2-dev
apt-get -y install libmariadb-dev-compat libmariadb-dev
apt-get -y install libfcgi-dev libfcgi0ldbl
apt-get -y install git build-essential
apt-get -y install autoconf re2c bison libxml2-dev -y
```

## 2. Setting up the target source code

AFL works by injecting "instrumentation" into the source code while compiling it. The creator of AFL  created a special compiler to accomplish this task. Now that we have AFL installed, we can compile our targets source code using this compiler.


The current source code is fresh from the repository. We could compile it with afl right and start fuzzing right away. However, thanks to some advice from [this article](https://www.tripwire.com/state-of-security/vert/fuzzing-php-for-fun-and-profit/), we can optimize our target furhter.


In the php source directory, open the file `./sapi/cli/php_cli.c` and go to line `1006`. Identify the following code.

```
		case PHP_MODE_CLI_DIRECT:
			cli_register_file_handles();
			if (zend_eval_string_ex(exec_direct, NULL, "Command line code", 1) == FAILURE) {
				exit_status=254;
			}
			break;
```

Replace it with this:

```
		case PHP_MODE_CLI_DIRECT:
			cli_register_file_handles();
			__AFL_INIT();
			while (__AFL_LOOP(100000))
				zend_eval_string_ex(exec_direct, NULL, "Command line code", 1);
			break;
```

The result should be a drastic speed improvement for fuzzing. 

Now we can go ahead and build php with afl:

``` bash
cd php-serc-php-7.0.33

./buildconf --force 
CC=afl-clang-fast CXX=afl-clang-fast++ ./configure 
```

This should take some time. If you get any errors, check to make sure all the dependencies we installed in section 1 are actually installed. 

Now to actually compile PHP. As PHP is a large project, compilation will probably take a large amount of time. 

To compile, run:

``` bash
AFL_USE_ASAN=1 make

# return to the base directory
cd ..
```
If it compiles without eror, move onto the next session. Otherwise, ensure you entered the commands described in this tutorial exactly.

## 3. Fuzzing First Steps

We're getting very close to actually fuzzing our target. Theres only a few things left to do.

First, we need to generate test cases. AFL will use the test cases that we bootstrap it with to run the target program. AFL will then mutate these test cases by flipping a bit or a byte and then run them again. Over time, it will identify the test cases that do interesting things with the program and generate more test cases that are like them. Test cases that don't do interesting things will be thrown out. Remember, a test case is simply an input to a particular part of the program.


Continuing with the guidance of the tutorial I mentioned before, we'll generate some serialized inputs to pass into PHP's *unserialize()* funciton. The reasoning behind using this function is because of the common class of vulnerabilities known as deserialization vulnerabilities.


Run the following commands from the project root directory to make a folder for the test cases and the test cases themselves:


``` bash
mkdir serialized_data && cd serialized_data
```

Per the tripwire guide mentioned earlier, we should generate some tests.

``` bash
../php-src-php-7.0.33/sapi/cli/php -r 'echo serialize("a");' > string
../php-src-php-7.0.33/sapi/cli/php -r 'echo serialize(1);' > number
../php-src-php-7.0.33/sapi/cli/php -r 'echo serialize([1,2]);' > array_of_num
../php-src-php-7.0.33/sapi/cli/php -r 'echo serialize(["1","2"]);' > array_of_str
../php-src-php-7.0.33/sapi/cli/php -r 'echo serialize([["1","2"],["3","4"],[1,2]]);' > array_of_array
```
From here we need to set a few last things up:

``` bash
# make sure youre in the project root directory
USE_ZEND_ALLOC=0 tmux

# configure core dump notifications
sudo su # enter your password
echo core >/proc/sys/kernel/core_pattern
exit
```

Finally, we can get into actually fuzzing the program itself. 

```
afl-fuzz -i serialized_data -o basic_fuzz -m none -- ./php-src-php-7.0.33/sapi/cli/php -r 'unserialize(file_get_contents("php://stdin"));'
```

Congrats, you are now fuzzing php!
I found my first crash within two minutes of starting the fuzz job.


 
## 4. Post  Fuzz
After running the fuzzer for just over seventeen hours, I ended the job. Over that time, AFL generated 897 crashes, with 17 of those being unique. Press `ctrl+c` to end your fuzz job, and investigate the `basic_fuzz` folder. Mine looks like this:

``` bash
crashes/
fuzz_bitmap
fuzzer_stats
hangs/
plot_data
queue/
```

You can see folders for crashes, hangs, and the input queue, along with some other files. The AFL readme explains the folders as follows:

```
  - queue/   - test cases for every distinctive execution path, plus all the
               starting files given by the user. This is the synthesized corpus
               mentioned in section 2.

               Before using this corpus for any other purposes, you can shrink
               it to a smaller size using the afl-cmin tool. The tool will find
               a smaller subset of files offering equivalent edge coverage.

  - crashes/ - unique test cases that cause the tested program to receive a
               fatal signal (e.g., SIGSEGV, SIGILL, SIGABRT). The entries are 
               grouped by the received signal.

  - hangs/   - unique test cases that cause the tested program to time out. The
               default time limit before something is classified as a hang is
               the larger of 1 second and the value of the -t parameter.
               The value can be fine-tuned by setting AFL_HANG_TMOUT, but this
               is rarely necessary.
```

### Next Steps

These initial crashes will likely not produce anything fruitful. A complete fuzzing job will involve stopping the fuzz after every few cycles, trimming the input queue, and resuming the fuzz. Crashes should be examined to determine whether they are a result of the injected instrumentation or an actual vulnerability. 95% of the crashes found intial will be a result of instrumentation and are essentially useless.

If a valid crash is found, it should be further examined by the researcher. If the researcher can maually reproduce the crash condition, a vulnerability has been found. 

# References
- [AFL Home](http://lcamtuf.coredump.cx/afl/)
- [AFL Readme](http://lcamtuf.coredump.cx/afl/README.txt)
- [AFL Quickstart](http://lcamtuf.coredump.cx/afl/QuickStartGuide.txt)
- [AFL Install Instructions](https://github.com/mirrorer/afl/blob/master/docs/INSTALL)
- [AFL Technical Whitepaper](http://lcamtuf.coredump.cx/afl/technical_details.txt)
- [LLVM Install](http://clang.llvm.org/get_started.html)
- [AFL PHP](https://www.tripwire.com/state-of-security/vert/fuzzing-php-for-fun-and-profit/)
- [Fuzzing Workflows: a fuzz job from start to finish](https://foxglovesecurity.com/2016/03/15/fuzzing-workflows-a-fuzz-job-from-start-to-finish/)

### Help Postings
- [Why the installed LLVM doesnt work](https://groups.google.com/forum/#!topic/afl-users/1WqZpGXvYY0)
- [Suggestion for Docker Container](https://groups.google.com/forum/#!topic/afl-users/D3-jbylxHeg)
- [Example of LLVM setup](https://volatileminds.net/2015/07/01/advanced-afl-usage.html)